<?php namespace App\Services;

use \Cache;

class RottenTomatoes
{
    static function search($dvd_title){

        //return from the cache if it exists
        if(Cache::has($dvd_title)){
            return Cache::get($dvd_title);
        }

        $root = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?';

        //build the url
        $params = http_build_query(array(
                "page"      => 1,
                "apikey"    => 'pyx3za4uznu4x4qjmnz4mdc3',
                "q"         => $dvd_title,
        ));

        //perform the query
        $url = $root . $params;
        $contents = file_get_contents($url);
        $json = json_decode($contents, true);
        $movies = $json["movies"];

        $movie_info = NULL;

        //find the right movie in the list
        if(count($movies) > 0){
            foreach($movies as $movie){
                if(trim($movie["title"]) == trim($dvd_title)){
                    $movie_info = $movie;
                }
            }
        }

        Cache::put($dvd_title, $movie_info, 60);

        return $movie_info;
    }
}
