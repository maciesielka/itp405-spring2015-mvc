<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dvd;
use App\Models\Review;
use App\Models\Format;
use App\Models\Genre;
use App\Models\Rating;
use App\Models\Sound;
use App\Models\Label;

class GenreController extends Controller
{

    function queryByGenre(Request $request){
        $genre_name = $request->route('genre_name');
        $dvds = Dvd::with('genre')
                    ->with('genre')
                    ->with('label')
                     ->whereHas('genre', function($query) use ($genre_name){
                         $query->where('genre_name', '=', $genre_name);
                     })
                    ->get();

        return view('genres', [
            "dvds"      => $dvds,
            "genre"     => $genre_name
        ]);
    }

}
