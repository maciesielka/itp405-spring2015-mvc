<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dvd;
use App\Models\Review;
use App\Models\Format;
use App\Models\Genre;
use App\Models\Rating;
use App\Models\Sound;
use App\Models\Label;

use App\Services\RottenTomatoes;

class DvdController extends Controller
{

    function __construct(){

    }

    function search(){
        $dvd = DVD::getInstance();
        return view('search', [
            "genres"    => $dvd->getGenres(),
            "ratings"   => $dvd->getRatings()
        ]);
    }


    function results(Request $request){
        $dvd = Dvd::getInstance();
        $dvds = [];

        //if there is no input
        if(empty($request->all())){
            $dvds = $dvd->getAll();
        }else{
            $dvds = $dvd->getDvds([
                "title"     => $request->input("title"),
                "genre_id"  => $request->input("genre"),
                "rating_id" => $request->input("rating")
            ]);
        }

        return view('results', [
            "dvds"          => $dvds,
            "genres"        => Genre::all()
        ]);
    }

    function reviews(Request $request){
        $id = $request->route("id");

        $dvdBase = Dvd::getInstance();
        $dvd = $dvdBase->performQuery($id);

        //grab movie-info from the RottenTomatoes service
        $movie_info = RottenTomatoes::search($dvd->title);

        return view('reviews', [
            "dvd"           => $dvd,
            "movie_info"    => $movie_info
        ]);
    }

    function addReview(Request $request){
        $id = $request->input('dvd_id');
        if($request->input("submit")){
            $data = [
                'title'         =>  $request->input('title'),
                'description'   =>  $request->input('description'),
                'rating'        =>  $request->input('rating'),
                'dvd_id'        =>  $request->input('dvd_id')
            ];

            $validation = Review::validate($data);

            if ($validation->passes()) {
                Review::makeInstance($data);

                return redirect("reviews/$id")->with('success', 'Song successfully saved');
            } else {
                // redirect to /songs/new with error messages and old input
                return redirect("reviews/$id")
                ->withInput()
                ->withErrors($validation);
            }
        }else{
            return redirect("dvds");
        }
    }

    function create(Request $request){
        return view('create_dvd', [
            "genres"    => Genre::all(),
            "sounds"    => Sound::all(),
            "ratings"   => Rating::all(),
            "labels"    => Label::all(),
            "formats"   => Format::all()
        ]);
    }

    function formSubmit(Request $request){
        $dvd = new Dvd();
        $dvd->title = $request->input('title');
        $dvd->release_date = $request->input('release_date');
        $dvd->genre_id = $request->input('genre');
        $dvd->label_id = $request->input('label');
        $dvd->rating_id = $request->input('rating');
        $dvd->sound_id = $request->input('sound');
        $dvd->format_id = $request->input('format');

        $validation = $dvd->validate();
        if($validation->passes()){
            $dvd->save();
            return redirect("dvds/create")->with('success', 'Dvd saved successfully!');
        }else{
            return redirect("dvds/create")
                ->withInput()
                ->withErrors($validation);
        }


    }
}
