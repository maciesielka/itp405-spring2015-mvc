<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/dvds', 'DvdController@results');
Route::post('/dvds', 'DvdController@formSubmit');
Route::get('/dvds/create', 'DvdController@create');
Route::get('/dvds/search', 'DvdController@search');

Route::get('/reviews/{id}', 'DvdController@reviews');
Route::post('/reviews/new', 'DvdController@addReview');

Route::get('/genres/{genre_name}/dvds', 'GenreController@queryByGenre');
