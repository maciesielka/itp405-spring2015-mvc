<?php namespace App\Models;

use DB;
use Validator;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Review extends Model
{

    function dvd(){
        return $this->belongsTo('App\Models\Dvd');
    }

    static function validate($input){
        return Validator::make($input, [
          'title' => 'required|string|min:5',
          'description' => 'required|string|min:20',
          'rating'  => 'integer',
          'dvd_id' => 'required|integer'
        ]);
    }

    public static function makeInstance($data)
    {
        return DB::table('reviews')->insert($data);
    }
}
