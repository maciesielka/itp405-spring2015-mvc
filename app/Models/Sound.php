<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Sound extends Model
{

    function dvd(){
        return $this->belongsTo('App\Models\Dvd', 'id', 'sound_id');
    }
}
