<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 *
 */
class Dvd extends Model
{


    /** ORM functions  **/
    function reviews(){
        return $this->hasMany('App\Models\Review');
    }

    function format(){
        return $this->hasOne('App\Models\Format', 'id', 'format_id');
    }

    function sound(){
        return $this->hasOne('App\Models\Sound', 'id', 'sound_id');
    }

    function rating(){
        return $this->hasOne('App\Models\Rating', 'id', 'rating_id');
    }

    function genre(){
        return $this->hasOne('App\Models\Genre', 'id', 'genre_id');
    }

    function label(){
        return $this->hasOne('App\Models\Label', 'id', 'label_id');
    }

    /** END ORM functions **/

    static function getInstance(){
        return new Dvd();
    }

    function getRatings(){
        $query = DB::table('ratings')->orderBy('rating_name', 'asc');
        return $query->get();
    }

    function getGenres(){
        $query = DB::table('genres')->orderBy('genre_name', 'asc');
        return $query->get();
    }

    function getDvds($params){
        $query = DB::table('dvds')
                    ->join('genres' , 'dvds.genre_id'   , '=', 'genres.id')
                    ->join('ratings', 'dvds.rating_id'  , '=', 'ratings.id')
                    ->join('labels' , 'dvds.label_id'   , '=', 'labels.id')
                    ->join('formats', 'dvds.format_id'  , '=', 'formats.id')
                    ->join('sounds' , 'dvds.sound_id'   , '=', 'sounds.id')
                    ->select(DB::raw("dvds.id, dvds.title, genres.genre_name, ratings.rating_name, labels.label_name, formats.format_name, sounds.sound_name, DATE_FORMAT(dvds.release_date, '%b %d %Y') AS release_date"))
                    ->orderBy('dvds.title', 'asc');

        if(!empty($params)){
            $query->where('dvds.title', 'LIKE', '%' . $params['title'] . '%');


            if($params['genre_id'] != 'all'){
                $query->where('dvds.genre_id', '=', $params['genre_id']);
            }
            if($params['rating_id'] != 'all'){
                $query->where('dvds.rating_id', '=', $params['rating_id']);
            }
        }

        return $query->get();
    }

    function getAll(){
        return $this->getDvds([]);
    }

    function performQuery($id){
        $query = DB::table('dvds')
                    ->join('genres' , 'dvds.genre_id'   , '=', 'genres.id')
                    ->join('ratings', 'dvds.rating_id'  , '=', 'ratings.id')
                    ->join('labels' , 'dvds.label_id'   , '=', 'labels.id')
                    ->join('formats', 'dvds.format_id'  , '=', 'formats.id')
                    ->join('sounds' , 'dvds.sound_id'   , '=', 'sounds.id')
                    ->select(DB::raw("dvds.id, dvds.title, genres.genre_name, ratings.rating_name, labels.label_name, formats.format_name, sounds.sound_name, DATE_FORMAT(dvds.release_date, '%b %d %Y') AS release_date"))
                    ->where('dvds.id', '=', "$id")
                    ->orderBy('dvds.title', 'asc');

        $results = $query->get();
        if(count($results) != 1){
            return null;
        }else{

            $dvd = $results[0];

            //grab the reviews
            $reviewQuery = DB::table('reviews')
                            ->where('reviews.dvd_id', '=', "$id")
                            ->select('*');
            $reviews = $reviewQuery->get();

            $dvd->reviews = $reviews;

            return $dvd;
        }
    }

    function validate(){
        return Validator::make($this->getAttributes(), [
          'title'           => 'required|string',
          'release_date'    => 'required|date',
          'format_id'       => 'required|integer',
          'sound_id'        => 'required|integer',
          'rating_id'       => 'required|integer',
          'genre_id'        => 'required|integer',
          'label_id'        => 'required|integer'
        ]);
    }
}
