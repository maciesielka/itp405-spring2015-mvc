<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Genre extends Model
{

    function dvd(){
        return $this->belongsTo('App\Models\Dvd', 'id', 'genre_id');
    }
}
