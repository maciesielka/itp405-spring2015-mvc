<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Rating extends Model
{

    function dvd(){
        return $this->belongsTo('App\Models\Dvd', 'id', 'rating_id');
    }
}
