<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Format extends Model
{

    function dvd(){
        return $this->belongsTo('App\Models\Dvd', 'id', 'format_id');
    }
}
