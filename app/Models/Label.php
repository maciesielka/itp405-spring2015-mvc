<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Label extends Model
{
    function dvd(){
        return $this->belongsTo('App\Models\Dvd', 'id', 'label_id');
    }
}
