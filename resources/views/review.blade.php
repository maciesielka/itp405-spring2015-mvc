<div class="row review">
    <div class="review-title col-xs-8"><h4>{{$review->title}}</h4></div>
    <div class="col-xs-4 text-center"><span class="review-rating">{{$review->rating}}</span>&nbsp;/&nbsp;10</div>
    <div class="review-description col-xs-12">{{$review->description}}</div>
</div>
