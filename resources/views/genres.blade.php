@extends('layout')

@section('styles')
@stop

@section('content')
<h1>Dvds that are of the "{{$genre}}" genre</h1>
<div class="results">
    <table class="table table-striped">
        <thead>
            <tr>
              <th>Title</th>
              <th>Rating</th>
              <th>Genre</th>
              <th>Label</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dvds as $dvd)
            <tr>
              <td>{{  $dvd->title }}</td>
              <td>{{  $dvd->rating->rating_name }}</td>
              <td>{{  $dvd->genre->genre_name }}</td>
              <td>{{  $dvd->label->label_name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@stop
