@extends('layout')

@section('styles')
<style>
.review-rating {
    color: red;
}

.dvd-container {
    margin-top: 50px;
}

.review-description {
    color: #888;
}

.reviews {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border: thin solid black;
    padding: 0 15px;
}

.review {

}

.review:nth-child(even){
    background-color: #eee;
}

.add-review-container {

    padding-bottom: 10px;
}

.error {
    color: red;
}

.success {
    color: green;
}

.dvd-cast-member
{
    color: #aaa;
}

.underline
{
    text-decoration: underline;
}

</style>

@stop

@section('content')

@if($dvd != null)

<div class="dvd-container">
    <div class="row">
        <div class="dvd-title col-xs-6"><h1>{{ $dvd->title }}</h1></div>
        <div class="dvd-picture-container col-xs-6">
            @if($movie_info["posters"]["thumbnail"])
                <img class="dvd-picture" src="{{ $movie_info["posters"]["thumbnail"] }}" />
            @else
                <div>No Picture Available</div>
            @endif
        </div>
        <div class="dvd-release col-xs-4">Released: {{ $dvd->release_date }}</div>
        <div class="dvd-label col-xs-4">Label: {{ $dvd->label_name }}</div>
        <div class="dvd-sound col-xs-4">Sound: {{ $dvd->sound_name }}</div>
        <div class="dvd-genre col-xs-4">Genre: {{ $dvd->genre_name }}</div>
        <div class="dvd-rating col-xs-4">Rated {{ $dvd->rating_name }}</div>
        <div class="dvd-format col-xs-4">Format: {{ $dvd->format_name }}</div>
        @if($movie_info != NULL)
        <div class="dvd-critics-score col-xs-4">
            Critics Score:
            @if($movie_info["ratings"]["critics_score"] != -1)
                 {{ $movie_info["ratings"]["critics_score"] . " - \"" . $movie_info["ratings"]["critics_rating"] . "\"" }}
            @else
                {{"N/A"}}
            @endif
        </div>
        <div class="dvd-audience-score col-xs-4">
            Audience Score:
            @if($movie_info["ratings"]["audience_score"] != -1)
                 {{ $movie_info["ratings"]["audience_score"] . " - \"" . $movie_info["ratings"]["audience_rating"] . "\"" }}
            @else
                {{"N/A"}}
            @endif
        </div>
        <div class="dvd-runtime col-xs-4">Runtime: {{ $movie_info["runtime"] }} min</div>
        <div class="dvd-abridged-cast col-xs-12">
            <span class="underline"> Abridged Cast </span>
            @foreach($movie_info["abridged_cast"] as $actor)
                <div class="dvd-cast-member">{{ $actor["name"] }}</div>
            @endforeach
        </div>
        @endif
    </div>
    <div class="row reviews">
        <div class="row">
            <h3 class="col-xs-12">Reviews</h3>
        </div>
        @foreach($dvd->reviews as $review)
            @include('review', array("review" => $review))
        @endforeach

        <div class="row add-review-container">
            <div class="col-xs-4 form">
                <h3>Write a review</h3>
                <form class="form" action="{{ url('/reviews/new')}}" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" type="text" name="title" value="">
                    </div>
                    <div class="form-group">
                        <label>Rating</label>
                        <select class="form-control" name="rating">
                            @foreach(range(1,10) as $value)
                                <option value="{{ $value }}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description"></textarea>
                    </div>
                    <input type="hidden" name="dvd_id" value="{{$dvd->id}}">

                    <input type="submit" class="form-control" name="submit" value="Submit" />
                </form>
                @foreach ($errors->all() as $errorMessage)
                <p class="error">{{ $errorMessage }}</p>
                @endforeach

                @if (Session::has('success'))
                <p class="success">{{ Session::get('success') }}</p>
                @endif
            </div>
        </div>
    </div>

</div>

@endif

@stop
