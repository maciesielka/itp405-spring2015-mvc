@extends('layout')

@section('styles')
<style>
.sidebar {
    background-color: #333;
    border-radius: 5px;
    padding: 15px;
}

.sidebar a {
    color: white;
}
</style>
@stop

@section('content')
<h1 class="text-center">Results From Your Fantastic Search</h1>
<div class="row">
    <div class="col-xs-2 sidebar">
        @foreach($genres as $genre)
            <div><a href="{{url('/genres/'.$genre->genre_name.'/dvds')}}">{{$genre->genre_name}}</a></div>
        @endforeach
    </div>
    <div class="col-xs-10 results">
        <table class="table table-striped">
            <thead>
                <tr>
                  <th>Title</th>
                  <th>Rating</th>
                  <th>Genre</th>
                  <th>Label</th>
                  <th>Sound</th>
                  <th>Format</th>
                  <th>Release Date</th>
                  <th>Reviews</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dvds as $dvd)
                <tr>
                  <td>{{  $dvd->title }}</td>
                  <td>{{  $dvd->rating_name }}</td>
                  <td>{{  $dvd->genre_name }}</td>
                  <td>{{  $dvd->label_name }}</td>
                  <td>{{  $dvd->sound_name }}</td>
                  <td>{{  $dvd->format_name }}</td>
                  <td>{{  $dvd->release_date }}</td>
                  <td><a href="{{  '/reviews/'.$dvd->id }}">Reviews</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
