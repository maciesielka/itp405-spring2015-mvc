@extends('layout')

@section('styles')
<style>
.success {
    background-color: green;
    color: white;
}

.error {
    background-color: red;
    color: white;
}
</style>

@stop

@section('content')

<h1>Create a new Dvd</h1>
@foreach ($errors->all() as $errorMessage)
<div class="error"><p>{{ $errorMessage }}</p></div>
@endforeach

@if (Session::has('success'))
<div class="success"><p>{{ Session::get('success') }}</p></div>
@endif
<form action="{{ url('dvds') }}" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="form-group">
        <label>Dvd Title</label>
        <input class="form-control" type="text" name="title" value="{{old('title')}}">
    </div>
    <div class="form-group">
        <label>Release Date</label>
        <input class="form-control" type="date" name="release_date" value="{{old('release_date')}}">
    </div>
    <div class="form-group">
        <label>Genre</label>
        <select class="form-control" name="genre">
            @foreach($genres as $genre)
                <option value="{{$genre->id}}" @if(old('genre') == $genre->id) selected @endif >{{$genre->genre_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Label</label>
        <select class="form-control" name="label">
            @foreach($labels as $label)
                <option value="{{$label->id}}" @if(old('label') == $label->id) selected @endif>{{$label->label_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Sound</label>
        <select class="form-control" name="sound">
            @foreach($sounds as $sound)
                <option value="{{$sound->id}}" @if(old('sound') == $sound->id) selected @endif>{{$sound->sound_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Rating</label>
        <select class="form-control" name="rating">
            @foreach($ratings as $rating)
                <option value="{{$rating->id}}" @if(old('rating') == $rating->id) selected @endif>{{$rating->rating_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Format</label>
        <select class="form-control" name="format">
            @foreach($formats as $format)
                <option value="{{$format->id}}" @if(old('format') == $format->id) selected @endif>{{$format->format_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <input class="form-control" type="submit" name="submit" value="Submit">
    </div>
</form>
@stop
